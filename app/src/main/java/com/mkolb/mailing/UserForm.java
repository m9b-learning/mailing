package com.mkolb.mailing;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.mkolb.db.DbHandler;
import com.mkolb.model.Person;

public class UserForm extends AppCompatActivity implements View.OnClickListener {

    private Button bSave, bCancel;
    private Spinner spinner;
    private EditText firstname, lastname, email;
    private Person person;
    private DbHandler dbh;
    private int id = 0;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_form);

        bSave = findViewById(R.id.bSave);
        bSave.setOnClickListener(this);

        bCancel = findViewById(R.id.bCancel);
        bCancel.setOnClickListener(this);

        spinner = findViewById(R.id.sCivility);
        firstname = findViewById(R.id.sFirstname);
        lastname = findViewById(R.id.sLastname);
        email = findViewById(R.id.sEmail);

        ArrayAdapter<CharSequence> data = ArrayAdapter.createFromResource(this, R.array.civility, android.R.layout.simple_spinner_item);
        data.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(data);

        dbh = new DbHandler(this);

        Bundle extras = getIntent().getExtras();

        if (extras == null) {

            spinner.setSelection(0);
            firstname.setText("");
            lastname.setText("");
            email.setText("");
            person = new Person();
        }
        else if (extras.getInt("id", id) != 0){

            id = extras.getInt("id");
            person = dbh.getPerson(id);

            spinner.setSelection(person.getCivility());
            firstname.setText(person.getFirstname());
            lastname.setText(person.getLastname());
            email.setText(person.getEmail());
        }
        else {
            Toast.makeText(getApplicationContext(), getResources().getText(R.string.problem), Toast.LENGTH_LONG).show();

            intent = new Intent(this, Home.class);
            startActivity(intent);
        }
    }

    @Override
    public void onClick(View v) {
        String message = "";

        if (v.equals(bSave)) {
            message = "click on Save";

            if (spinner.getSelectedItemPosition() > 0
                    && firstname.getText().length() != 0
                    && lastname.getText().length() != 0
                    && email.getText().length() != 0) {

                person = new Person(
                        id,
                        spinner.getSelectedItemPosition(),
                        firstname.getText().toString(),
                        lastname.getText().toString(),
                        email.getText().toString()
                );

                if (id != 0) {
                    dbh.updatePerson(person);
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.updateSuccess), Toast.LENGTH_LONG).show();

                    intent = new Intent(this, MailList.class);
                    startActivity(intent);
                } else {
                    dbh.addPerson(person);
                    Toast.makeText(getApplicationContext(), getResources().getText(R.string.addSuccess), Toast.LENGTH_LONG).show();

                    intent = new Intent(this, Home.class);
                    startActivity(intent);
                }
            } else {
                Toast.makeText(getApplicationContext(), getResources().getText(R.string.fail), Toast.LENGTH_LONG).show();
            }

        } else if (v.equals(bCancel)) {
            message = "click on Cancel";
            firstname.setText("");
            lastname.setText("");
            email.setText("");
            Toast.makeText(getApplicationContext(), getResources().getText(R.string.cancel), Toast.LENGTH_LONG).show();

            intent = new Intent(this, Home.class);
            startActivity(intent);
        }

        Log.d("Mailing.DEBUG", message);

    }
}
