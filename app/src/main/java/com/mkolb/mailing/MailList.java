package com.mkolb.mailing;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.mkolb.db.DbHandler;
import com.mkolb.model.Person;

public class MailList extends AppCompatActivity {

    public DbHandler db = null;

    private ListView people;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_list);

        people = findViewById(R.id.PersonList);

        db = new DbHandler(this);

        final ArrayAdapter<Person> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, db.getAllPeople());
        people.setAdapter(adapter);
        people.setClickable(true);

        people.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Person person = adapter.getItem(position);
                Intent intent = new Intent(MailList.this, UserForm.class);
                intent.putExtra("id", person.getId());
                startActivity(intent);
            }
        });
    }

}
