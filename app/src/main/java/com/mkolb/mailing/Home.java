package com.mkolb.mailing;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class Home extends AppCompatActivity implements View.OnClickListener {

    private Button bList, bAdd, bClose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        bList = findViewById(R.id.bList);
        bList.setOnClickListener(this);

        bAdd = findViewById(R.id.bAdd);
        bAdd.setOnClickListener(this);

        bClose = findViewById(R.id.bClose);
        bClose.setOnClickListener(this);

    }

    public void onClick(View v) {
        String message = "";
        Intent intent = null;

        if (v.equals(bList)) {
            message = "clic on list";
            intent = new Intent(this, MailList.class);
        }
        else if (v.equals(bAdd)) {
            message = "clic on add";
            intent = new Intent(this, UserForm.class);
        }
        else if (v.equals(bClose)) {
            finishAffinity();
            System.exit(0);
        }

        startActivity(intent);
        Log.d("Mailing.DEBUG", message);
    }
}
