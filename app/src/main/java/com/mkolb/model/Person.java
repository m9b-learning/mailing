package com.mkolb.model;

import com.mkolb.helper.PersonHelper;

public class Person {

    private int id, civility;
    private String firstname, lastname, email;

    public Person() {
        this.id = 0;
        this.civility = 0;
        this.firstname = "";
        this.lastname = "";
        this.email = "";
    }

    public Person(int id, int civility, String firstname, String lastname, String email) {
        this.id = id;
        this.civility = civility;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public int getCivility() {
        return civility;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCivility(int civility) {
        this.civility = civility;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {

        StringBuilder sb = new StringBuilder();
        sb.append(PersonHelper.getCivilityDisplay(this.civility));
        sb.append(" ");
        sb.append(this.firstname);
        sb.append(" ");
        sb.append(this.lastname);
        sb.append("\n");
        sb.append(this.email);

        return sb.toString();
    }
}
