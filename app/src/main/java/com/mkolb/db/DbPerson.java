package com.mkolb.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mkolb.model.Person;

import java.util.ArrayList;
import java.util.List;

public abstract class DbPerson {

    public static final String TABLE_NAME = "person";
    public static final String FIELD_ID = "id";
    public static final String FIELD_CIVILITY = "civility";
    public static final String FIELD_FIRSTNAME = "firstname";
    public static final String FIELD_LASTNAME = "lastname";
    public static final String FIELD_EMAIL = "email";

    private static StringBuilder SQLQuery = null;

    private static final String LOGTAG = "DbMailingPerson";


    public static void createTable(SQLiteDatabase db) {

        Log.d(LOGTAG, "Création de la table " + TABLE_NAME);

        SQLQuery = new StringBuilder();
        SQLQuery.append("CREATE TABLE ");
        SQLQuery.append(TABLE_NAME);
        SQLQuery.append("(");
        SQLQuery.append(FIELD_ID);
        SQLQuery.append(" INT NOT NULL PRIMARY KEY, ");
        SQLQuery.append(FIELD_CIVILITY);
        SQLQuery.append(" INT NOT NULL, ");
        SQLQuery.append(FIELD_FIRSTNAME);
        SQLQuery.append(" VARCHAR(50) NOT NULL, ");
        SQLQuery.append(FIELD_LASTNAME);
        SQLQuery.append(" VARCHAR(50) NOT NULL, ");
        SQLQuery.append(FIELD_EMAIL);
        SQLQuery.append(" VARCHAR(250) NOT NULL ");
        SQLQuery.append(");");

        db.execSQL(SQLQuery.toString());
    }

    public static void dropTable(SQLiteDatabase db) {
        Log.d(LOGTAG, "Suppression de la table " + TABLE_NAME);

        SQLQuery = new StringBuilder();
        SQLQuery.append("DROP TABLE ");
        SQLQuery.append(TABLE_NAME);

        db.execSQL(SQLQuery.toString());
    }

    public static long insertPerson (SQLiteDatabase db, Person person) {
        Log.d(LOGTAG, "Insertion dans la table " + TABLE_NAME);

        person.setId(getNewId(db));
        return db.insert(TABLE_NAME, null, personMapper(person));
    }

    public static int updatePerson (SQLiteDatabase db, Person person) {
        Log.d(LOGTAG, "mise à jour dans la table " + TABLE_NAME);

        return db.update(TABLE_NAME, personMapper(person),  FIELD_ID + " = " + person.getId(), null);
    }

    private static ContentValues personMapper(Person person) {
        ContentValues values = new ContentValues();
        values.put(FIELD_ID, person.getId());
        values.put(FIELD_CIVILITY, person.getCivility());
        values.put(FIELD_FIRSTNAME, person.getFirstname());
        values.put(FIELD_LASTNAME, person.getLastname());
        values.put(FIELD_EMAIL, person.getEmail());

        return values;
    }

    private static int getNewId(SQLiteDatabase db){
        Log.d(LOGTAG, "Création d'un nouvel identifiant");

        Cursor SQLResult = db.rawQuery("SELECT IFNULL(MAX(" + FIELD_ID + "), 0) + 1 FROM " + TABLE_NAME, null);
        int retVal = 0;
        if (SQLResult != null){
            SQLResult.moveToFirst();
            retVal = SQLResult.getInt(0);
            SQLResult.close();
        }
        return retVal;
    }

    public static int deletePerson(SQLiteDatabase db, int id) {
        Log.d(LOGTAG, "Suppression dans la table " + TABLE_NAME);

        return db.delete(TABLE_NAME, FIELD_ID + " = " + id, null);
    }

    public static Person getPerson(SQLiteDatabase db, int id) {

        Log.d(LOGTAG, "Sélection à partir d'un identifiant dans la table " + TABLE_NAME);

        Person person = null;

        SQLQuery = new StringBuilder();
        SQLQuery.append("SELECT ");
        SQLQuery.append(FIELD_ID);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_CIVILITY);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_FIRSTNAME);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_LASTNAME);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_EMAIL);
        SQLQuery.append(" FROM ");
        SQLQuery.append(TABLE_NAME);
        SQLQuery.append(" WHERE ");
        SQLQuery.append(FIELD_ID);
        SQLQuery.append(" = ");
        SQLQuery.append(id);
        SQLQuery.append(" ORDER BY ");
        SQLQuery.append(FIELD_LASTNAME);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_FIRSTNAME);


        Cursor SQLResult = db.rawQuery(SQLQuery.toString(), null);
        if (SQLResult != null){
            SQLResult.moveToFirst();
            person = new Person(
                    SQLResult.getInt(0),
                    SQLResult.getInt(1),
                    SQLResult.getString(2).trim(),
                    SQLResult.getString(3).trim(),
                    SQLResult.getString(4).trim()
            );
        }

        SQLResult.close();
        return person;
    }

    public static List<Person> getAllPeople(SQLiteDatabase db) {
        Log.d(LOGTAG, "Sélection de toute la table " + TABLE_NAME);
        List<Person> people = new ArrayList<Person>();

        SQLQuery = new StringBuilder();
        SQLQuery.append("SELECT ");
        SQLQuery.append(FIELD_ID);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_CIVILITY);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_FIRSTNAME);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_LASTNAME);
        SQLQuery.append(", ");
        SQLQuery.append(FIELD_EMAIL);
        SQLQuery.append(" FROM ");
        SQLQuery.append(TABLE_NAME);

        Cursor SQLResult = db.rawQuery(SQLQuery.toString(), null);

        if (SQLResult.moveToFirst()) {
            do {
                Person person = new Person(
                        SQLResult.getInt(0),
                        SQLResult.getInt(1),
                        SQLResult.getString(2).trim(),
                        SQLResult.getString(3).trim(),
                        SQLResult.getString(4).trim()
                );
                people.add(person);
            } while (SQLResult.moveToNext());
        }
        SQLResult.close();

        return people;
    }

    public static int count(SQLiteDatabase db) {
        return db.rawQuery("SELECT COUNT(" + FIELD_ID + ") FROM " + TABLE_NAME, null).getInt(0);
    }

}
