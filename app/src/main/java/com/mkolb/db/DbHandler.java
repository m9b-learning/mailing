package com.mkolb.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.mkolb.model.Person;

import java.util.List;

public class DbHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "mailing.db";

    public DbHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        DbPerson.createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        DbPerson.dropTable(db);
        DbPerson.createTable(db);
    }

    public int getDbVersion() {
        return getReadableDatabase().getVersion();
    }

    public long addPerson(Person person) {

        return DbPerson.insertPerson(getWritableDatabase(), person);
    }

    public int  updatePerson(Person person) {
        return DbPerson.updatePerson(getWritableDatabase(), person);
    }

    public Person getPerson(int id) {

        return DbPerson.getPerson(getWritableDatabase(), id);
    }

    public List<Person> getAllPeople() {

        return DbPerson.getAllPeople(getWritableDatabase());
    }

}
