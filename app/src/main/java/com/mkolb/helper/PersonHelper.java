package com.mkolb.helper;

import android.app.Application;
import android.content.Context;
import com.mkolb.mailing.R;

public class PersonHelper extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
    }

    public static String getCivilityDisplay(int index) {
        String[] display = context.getResources().getStringArray(R.array.civility);
        return display[index];
    }
}
